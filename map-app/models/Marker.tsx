import { Realm, createRealmContext } from "@realm/react";

export class Marker extends Realm.Object {
  id!: Realm.BSON.ObjectId;
  latitude!: number;
  longitude!: number;
  imgUri!: string;

  static generate(latitude: number, longitude: number, imgUri: string) {
    return {
      id: new Realm.BSON.ObjectId(),
      latitude: latitude,
      longitude: longitude,
      imgUri: imgUri,
    };
  }

  static schema = {
    name: "Marker",
    primaryKey: "id",
    properties: {
      id: "objectId",
      latitude: "float",
      longitude: "float",
      imgUri: { type: "string", default: undefined },
    },
  };
}

const config = {
  schema: [Marker],
};

export default createRealmContext(config);
