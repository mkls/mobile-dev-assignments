import { useEffect, createContext } from "react";
import { StyleSheet, Dimensions, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import MapView from "react-native-maps";
import Map from "./components/Map";
import Details from "./components/Details";
import * as Notifications from "expo-notifications";
import * as Location from "expo-location";
import { AppContext } from "./components/Context";
import { MarkerData } from "./components/Types";

const Stack = createNativeStackNavigator();

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

export default function App() {
  let latitude = 58.009497637931254;
  let longitude = 56.2260306380644;
  let markers: never | MarkerData[] = [];

  useEffect(() => {
    const f = async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      let locationSubscrition = null;

      let notificationsStatus = await Notifications.requestPermissionsAsync();

      if (status == "granted") {
        let location = await Location.getCurrentPositionAsync();
        latitude = location.coords.latitude;
        longitude = location.coords.longitude;

        Notifications.addNotificationReceivedListener(handleNotification);

        let foregroundSubscrition = Location.watchPositionAsync(
          {
            accuracy: Location.Accuracy.High,
            distanceInterval: 5,
          },
          (location) => {
            let minDistance = 99999999;
            let closestMarker: MarkerData | null = null;

            markers.forEach((x, i) => {
              let distance = Math.sqrt(
                Math.pow(location.coords.latitude - x.latitude, 2) +
                  Math.pow(location.coords.longitude - x.longitude, 2)
              );

              if (distance < minDistance) {
                minDistance = distance;
                closestMarker = x;
              }
            });

            if (
              closestMarker !== null &&
              minDistance < 0.005 &&
              notificationsStatus.granted
            ) {
              const content = {
                title: "You are near something!",
                body: `Marker ${closestMarker.key}, distance: ${minDistance}`,
              };

              Notifications.scheduleNotificationAsync({
                content: content,
                trigger: { seconds: 1 },
              });
            } else {
              Notifications.dismissAllNotificationsAsync();
            }
          }
        );
      }
    };

    f();
  }, []);

  const handleNotification = (notification: Notification) => {
    Notifications.dismissAllNotificationsAsync();
  };

  return (
    <NavigationContainer>
      <AppContext.Provider value={markers}>
        <Stack.Navigator>
          <Stack.Screen
            name="Main"
            component={Map}
            initialParams={{
              latitude: latitude,
              longitude: longitude,
              latitudeDelta: 0.02,
              longitudeDelta: 0.02,
            }}
          />
          <Stack.Screen name="Details" component={Details} />
        </Stack.Navigator>
      </AppContext.Provider>
    </NavigationContainer>
  );
}
