import type { NativeStackScreenProps } from "@react-navigation/native-stack";

interface MapProps {
  latitude: number;
  longitude: number;
  latitudeDelta: number;
  longitudeDelta: number;
  connection: any;
}

interface DetailsProps {
  itemId: number;
}

export type MarkerData = {
  key: number;
  latitude: number;
  longitude: number;
  imgUri: string | undefined;
};

type ScreenPropList = {
  Map: MapProps;
  Details: DetailsProps;
};

export interface MapMarkers {
  markers: MarkerData[];
}

export interface DetailsState {
  itemId: number;
  imgUri: string | undefined;
}

export type MapScreenProps = NativeStackScreenProps<ScreenPropList, "Map">;
export type DetailsScreenProps = NativeStackScreenProps<
  ScreenPropList,
  "Details"
>;
