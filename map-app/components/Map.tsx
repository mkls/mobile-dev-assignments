import React from "react";
import MapView, { Marker, MapEvent } from "react-native-maps";
import { StyleSheet } from "react-native";
import type { MapScreenProps, MarkerData, MapMarkers } from "./Types";
import { AppContext } from "./Context";

export default class Map extends React.Component<MapScreenProps, MapMarkers> {
  counter = 0;

  latitude: number;
  longitude: number;
  latitudeDelta: number;
  longitudeDelta: number;
  connection: any;

  static contextType = AppContext;

  addMarker(e: MapEvent) {
    this.counter++;

    const newMarker = {
      key: this.counter,
      latitude: e.nativeEvent.coordinate.latitude,
      longitude: e.nativeEvent.coordinate.longitude,
      imgUri: undefined,
    };
    this.context.push(newMarker);
    this.setState(this.state);
  }

  onMarkerClick(e: MapEvent) {
    this.props.navigation.navigate("Details", {
      itemId: Number(e.nativeEvent.id),
    });
  }

  constructor(props: MapScreenProps) {
    super(props);

    this.state = {
      markers: this.context,
    };

    this.latitude = props.route.params.latitude;
    this.longitude = props.route.params.longitude;
    this.latitudeDelta = props.route.params.longitudeDelta;
    this.longitudeDelta = props.route.params.longitudeDelta;
    this.connection = props.route.params.connection;

    this.addMarker = this.addMarker.bind(this);
    this.onMarkerClick = this.onMarkerClick.bind(this);
  }

  render() {
    return (
      <MapView
        style={styles.map}
        initialRegion={{
          latitude: this.latitude,
          longitude: this.longitude,
          latitudeDelta: this.latitudeDelta,
          longitudeDelta: this.longitudeDelta,
        }}
        showsUserLocation={true}
        zoomControlEnabled={true}
        onPress={this.addMarker}
      >
        {this.context.map((marker) => (
          <Marker
            key={marker.key}
            identifier={`${marker.key}`}
            coordinate={{
              latitude: marker.latitude,
              longitude: marker.longitude,
            }}
            onPress={this.onMarkerClick}
          />
        ))}
      </MapView>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    flex: 1,
    alignSelf: "stretch",
  },
});
