import React from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import * as ImagePicker from "expo-image-picker";
import type { DetailsScreenProps, DetailsState } from "./Types";
import { AppContext } from "./Context";

export default class Details extends React.Component<
  DetailsScreenProps,
  DetailsState
> {
  static contextType = AppContext;
  itemId: number;

  constructor(props: DetailsScreenProps) {
    super(props);

    this.itemId = this.props.route.params.itemId;
    this.onImagePickerAsync = this.onImagePickerAsync.bind(this);
  }

  async onImagePickerAsync() {
    let permissionResult =
      await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (permissionResult.granted === false) {
      alert("Permission to access camera roll is required!");
      return;
    }

    let pickerResult = await ImagePicker.launchImageLibraryAsync();

    if (pickerResult.cancelled === true) {
      return;
    }

    this.context[this.itemId - 1].imgUri = pickerResult.uri;
    this.forceUpdate();
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Text>Details Screen</Text>
        <Text>itemId: {this.props.route.params.itemId}</Text>

        <Image
          source={{ uri: this.context[this.itemId - 1].imgUri }}
          style={{
            width: 300,
            height: 500,
            resizeMode: "contain",
            margin: 50,
          }}
        />

        <TouchableOpacity
          onPress={this.onImagePickerAsync}
          style={styles.button}
        >
          <Text style={styles.buttonText}>Pick an image</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = {
  button: {
    backgroundColor: "blue",
    padding: 15,
    borderRadius: 5,
  },

  buttonText: {
    fontSize: 14,
    color: "#fff",
  },
};
